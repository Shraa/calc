FROM python:3.7-buster

LABEL \
    version="2.0" \
    author="Mikhail Velkin <twilight@solfoto.ru>" \
    maintainer="Mikhail Velkin <twilight@solfoto.ru>" \
    description="Testcase for livness-, readiness and startup probes" \
    source="https://gitlab.com/Shraa/calc"

ARG TZ=Europe/Moscow
ARG DEBIAN_FRONTEND=noninteractive
ARG DEBCONF_NONINTERACTIVE_SEEN=true

ADD assets/ /

RUN set -ex \
 && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
 && dpkg-reconfigure tzdata \
 && apt-get update -y \
 && apt-get upgrade -yqq \
 && python3 -m pip install -r app/requirements.txt \
 && rm -rf \
      /var/lib/apt/lists/* \
      /tmp/* \
      /var/tmp/* \
      /usr/share/man \
      /usr/share/doc \
      /usr/share/doc-base

ENTRYPOINT ["/entrypoint.sh"]

CMD ["run"]
