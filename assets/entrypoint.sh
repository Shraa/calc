#!/usr/bin/env bash

if [ "${1}" == "run" ]; then
  if [ ! -f /tmp/started ]; then
    touch /tmp/started
  fi

  sleep 5

  python3 app/main.py

  if [ -f /tmp/started ]; then
    rm /tmp/started
  fi

fi
