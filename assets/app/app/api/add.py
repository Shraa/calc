# -*- coding: utf-8 -*-

def search(**kwargs) -> dict:
    if 'left' in kwargs and 'right' in kwargs:
        return dict(result=kwargs.pop('left')+kwargs.pop('right')), 200
    else:
        return dict(), 400
