# -*- coding: utf-8 -*-

import unittest

from tests import do_get, do_random


class TestAddService(unittest.TestCase):
    def setUp(self):
        self.url = 'http://localhost:5000'
        self.service = 'add'

    def test_add(self):
        left = do_random()
        right = do_random()
        ret = do_get('{0}/{1}?left={2}&right={3}'.format(self.url, self.service, left, right))
        self.assertEqual(ret.status_code, 200)
        self.assertEqual(ret.json(), dict(result=left+right))

    def test_add_no_left(self):
        right = do_random()
        ret = do_get('{0}/{1}?right={2}'.format(self.url, self.service, right))
        self.assertEqual(ret.status_code, 400)
        self.assertEqual(ret.json(), dict(status=400, title='Bad Request', type='about:blank', detail="Missing query parameter 'left'"))

    def test_add_no_right(self):
        left = do_random()
        ret = do_get('{0}/{1}?left={2}'.format(self.url, self.service, left))
        self.assertEqual(ret.status_code, 400)
        self.assertEqual(ret.json(), dict(status=400, title='Bad Request', type='about:blank', detail="Missing query parameter 'right'"))


if __name__ == '__main__':
    unittest.main()
