# -*- coding: utf-8 -*-

import random
import requests


def do_get(url: str):
    return requests.get(url='{}'.format(url))


def do_random():
    return random.random()
