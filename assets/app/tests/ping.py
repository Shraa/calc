# -*- coding: utf-8 -*-

import unittest

from random import choice
from string import ascii_letters

from tests import do_get


class TestPingService(unittest.TestCase):
    def setUp(self):
        self.url = 'http://localhost:5000'
        self.service = 'ping'

    def test_ping(self):
        ret = do_get('{0}/{1}'.format(self.url, self.service))
        self.assertEqual(ret.json(), dict())
        self.assertEqual(ret.status_code, 200)


if __name__ == '__main__':
    unittest.main()
