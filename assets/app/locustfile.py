import random

from locust import HttpUser, task, between


class MyHttpUser(HttpUser):
    wait_time = between(0, 1)

    @task
    def ping(self):
        self.client.get("/ping")

    @task(5)
    def add(self):
        self.client.get(f"/add?left={0}&right={1}".format(random.random(), random.random()), name="/add")
